---
title: Score Tennis
subtitle: How to Guide
date: 2017-12-04
tags: ["Tennis", "Guide"]
---
When you open the app, you get option to Login. 
You can login either with Google account or facebook account. Note: If these underlying accounts are different emails Ids then they will be two separate users.
Continue as Guest: does not give ability to access score on whatsscore.com and does not keep the history of scores.
It is better to login with ID of the player. If you are parent, choose to create account for your Kid so it can be tracked with his ID

{{< figure thumb="-thumb" link="/img/score-tennis/triangle.jpg" caption="Sphere" >}}

2. When you login, you can see
If this is your first time, you will see empty list.
Click on the “Add new match”
List of matches that you have scored in the past. 
Score of the matches already completed is displayed. Click on the match to navigate to statistics.
Matches that are entered but scheduled to be scored later. Click on the match to start scoring.

